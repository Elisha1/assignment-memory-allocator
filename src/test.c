#define _DEFAULT_SOURCE
#include "test.h"
#include <unistd.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug(const char *fmt, ...);
static void* init_heap();
static void create_new_heap(struct block_header *last_block);
static struct block_header* block_frm_cont(void * data);

static const size_t HEAP_SIZE = 10000;

void test1(struct block_header *first_block) {
    void *data = _malloc(1000);
    if (data == NULL) {
        err("Test 1: failed. _malloc return NULL.");
    }
    debug_heap(stdout, first_block);
    if (first_block->is_free != false || first_block->capacity.bytes != 1000) {
        err("Test 1: failed. capacity or is_free are incorrect.");
    }
    _free(data);
}

void test2(struct block_header *first_block) {
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    if (data1 == NULL || data2 == NULL) {
        err("Test 2: failed. _malloc return NULL.");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data2_block = block_frm_cont(data2);
    if (data1_block->is_free == false) {
        err("Test 2: failed. Status 'free' is false.");
    }
    if (data2_block->is_free == true) {
        err("Test 2: failed. Status 'free' is true but block is not free.");
    }
    _free(data1);
    _free(data2);
}

void test3(struct block_header *first_block) {
    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Test 3: failed. _malloc return NULL.");
    }
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data3_block = block_frm_cont(data3);
    if (data1_block->is_free == false) {
        err("Test 3: failed. Status 'free' is false.");
    }
    if (data3_block->is_free == true) {
        err("Test 3: failed. Status 'free' is true but block is not free.");
    }
    if (data1_block->capacity.bytes != 2000 + offsetof(struct block_header, contents)) {
        err("Test 3: failed. Block size is not correct.");
    }
    _free(data1);
    _free(data2);
    _free(data3);
}

void test4(struct block_header *first_block) {
    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        err("Test 4: failed. _malloc return NULL.");
    }
    struct block_header *addr = first_block;
    while (addr->next != NULL) addr = addr->next;
    create_new_heap(addr);
    void *data2 = _malloc(100000);
    debug_heap(stdout, first_block);
    struct block_header *data2_block = block_frm_cont(data2);
    if (data2_block == addr) {
        err("Test 4: failed. _created block near with first block heap.");
    }
    _free(data1);
    _free(data2);
}

void test() {
    struct block_header *first_block = (struct block_header*) init_heap();
    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    debug("Tests have finished successfully\n");
}

static void* init_heap() {
    void *heap = heap_init(HEAP_SIZE);
    if (heap == NULL) {
        err("Can't inicialize heap for tests.");
    }
    return heap;
}

static void create_new_heap(struct block_header *last_block) {
    struct block_header *addr = last_block;
    void* test_addr = (uint8_t*) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = mmap( (uint8_t*) (getpagesize() * ((size_t) test_addr / getpagesize() +
                                       (((size_t) test_addr % getpagesize()) > 0))), 1000,
          PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,0, 0);
    debug(test_addr);
}

static struct block_header* block_frm_cont(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}
